const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const sfet = require('simple-free-encryption-tool');

const OTP_TABLE = process.env.OTP_TABLE_NAME;
// TODO set with environment variable using https://github.com/zeit/ms
const OTP_TTL_IN_SECONDS = 60*5;
const OTP_EXPIRATION_MESSAGE = "OTP has expired";

// TODO add reinstantiation logic if env variables updated
const mailgun = require('mailgun-js')({
    apiKey: process.env.MAILGUN_API_KEY,
    domain: process.env.MAILGUN_DOMAIN
});

let self = {
    createEntry: ({ email, purpose, accountType }) => {
        return new Promise((resolve, reject) => {
            let otp = sfet.utils.randomstring.generate(6);
            dynamodb.put({
                TableName: OTP_TABLE,
                Item: {
                    "email": email,
                    "otp": otp,
                    "purpose": purpose,
                    "accountType": accountType,
                    "expiration": Math.floor(Date.now() / 1000) + OTP_TTL_IN_SECONDS
                }
            }).promise()
            .then(() => { resolve(otp); })
            .catch(reject);
        });
    },
    deleteEntry: ({ email, otp }) => {
        return new Promise((resolve, reject) => {
            dynamodb.delete({
                TableName: OTP_TABLE,
                Key: {
                    "email": email,
                    "otp": otp
                }
            }).promise()
            .then(resolve)
            .catch(reject);
        });
    },
    getEntry: ({ email, otp }) => {
        return new Promise((resolve, reject) => {
            let otpRetrievalFailure = (err) => {
                console.error(err);
                reject(new Error(OTP_EXPIRATION_MESSAGE));
            };
            dynamodb.get({
                TableName: OTP_TABLE,
                Key: {
                    "email": email,
                    "otp": otp
                }
            }).promise()
            .then(result => {
                result = result.Item;
                if (!result) {
                    otpRetrievalFailure(new Error("otp not found"));
                } else {
                    // validate expiration time
                    let currentTime = Math.floor(Date.now() / 1000);
                    if (currentTime > result.expiration) {
                        otpRetrievalFailure(
                            new Error(`otp expired: ${currentTime} > ${result.expiration}`)
                        );
                    } else {
                        resolve(result);
                    }
                }
            })
            .catch(otpRetrievalFailure);
        });
    },
    send: ({ email, subject, body }) => {
        console.log(`otp sending email '${subject}' to ${email}`);
        return new Promise((resolve, reject) => {
            let message = {
                from: process.env.MAILGUN_FROM,
                to: email,
                subject: subject,
                text: body
            };

            mailgun.messages().send(message, function (err, body) {
                if (err){
                    reject(err);
                } else {
                    resolve();
                }
            });
        });
    }
};

module.exports = self;