const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();
const moment = require('moment');

const TRANSACTIONS_TABLE = process.env.TRANSACTIONS_TABLE_NAME;

const ALLOWED_FIELDS = {
    transactionId: "string",
    transactionDate: "string",
    description: "string",
    amount: "integer",
    currency: "string",
    labels: "string array",
    masked: "boolean"
};

let getObjectLength = (obj) => {
    let size = 0;
    for (let key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
}

let validateTransactions = function(transactions) {
    console.log(`validateTransactions called on ${JSON.stringify(transactions)}`);
    let errors = [];
    transactionIds = {};

    for (let i in transactions) {
        let transaction = transactions[i];
        let error = {
            "transactionId": transaction.transactionId,
            "description": "TRANSACTION_DESCRIPTION",
            "errors": []
        };

        // verify transaction id available
        if (!transaction.transactionId) {
            error.errors.push(`transaction ID missing`);
        } else {
            // verify transaction id unique (for this batch)
            if (transactionIds[transaction.transactionId]) {
                error.errors.push(`duplicate transaction ID`);
            }
            transactionIds[transaction.transactionId] = true;
        }

        // date missing
        if (!transaction.transactionDate) {
            error.errors.push(`transaction date missing`);
        } else {
            // date invalid
            if (!moment(transaction.transactionDate, 'YYYYMMDD', true).isValid()) {
                error.errors.push(`transaction date invalid`);
            }
        }

        // description missing
        if (!transaction.description) {
            error.errors.push(`description missing`);
        }

        // amount missing
        if (!transaction.amount) {
            error.errors.push(`amount missing`);
        } else {
            // amount invalid
            if (!Number.isInteger(transaction.amount)) {
                error.errors.push(`amount is not an integer`);
            }
        }

        // currency
        if (!transaction.currency) {
            error.errors.push(`currency missing`);
        }

        // labels
        if (transactions.labels) {
            if (!Array.isArray(transactions.labels)) {
                error.errors.push(`labels must be an array of strings`);
            } else {
                let isInvalid = false;
                for (let label in transactions.labels) {
                    if (typeof label != 'string') {
                        isInvalid = true;
                    }
                }
                if (isInvalid) {
                    error.errors.push(`labels must be an array of strings`);
                }
            }
        }

        // description masking
        if (transaction.masked) {
            if (typeof transaction.masked != "boolean"){
                error.errors.push(`masked must be a boolean value`);
            }
        }

        if (error.errors.length > 0) {
            errors.push(error);
        }
    }

    if (errors.length > 0) {
        console.error('validation errors:');
        console.error(errors);
    }

    return errors;
}

let self = {
    addOrUpdateEntries: ({ accountId, transactions }) => {
        return new Promise((resolve, reject) => {
            console.log(`transactions.addOrUpdateEntries called for account ${accountId} with transactions ${JSON.stringify(transactions)}`);
            let validationErrors = validateTransactions(transactions);
            if (validationErrors.length > 0) {
                resolve({
                    accountId,
                    validationErrors,
                    successes: 0,
                    failures: transactions.length
                });
                return;
            }

            // now that we've validated the transactions, they need to be reorganized
            // and grouped by date with their transactionIds as keys
            let transactionsByDate = {};
            for (let t in transactions) {
                let transaction = transactions[t];
                // ensure all required values are set
                transaction.labels = transaction.labels || [];
                transaction.masked = transaction.masked || false;

                transactionsByDate[transaction.transactionDate] = transactionsByDate[transaction.transactionDate] || {};
                let cleanTransactionObject = {};
                for (let key in ALLOWED_FIELDS) {
                    cleanTransactionObject[key] = transaction[key];
                }
                // remove date
                delete cleanTransactionObject['transactionDate'];
                transactionsByDate[transaction.transactionDate][transaction.transactionId] = cleanTransactionObject;
            }

            // upload for each date
            let promises = [];
            for (let transactionDate in transactionsByDate) {
                promises.push(self.addOrUpdateEntry({
                    accountId,
                    transactionDate: Number(transactionDate),
                    transactions: transactionsByDate[transactionDate]
                }));
            }

            Promise.all(promises)
            .then(results => {
                let failedTransactionDates = [];
                let successes = 0;
                let failures = 0;
                for (let i in results) {
                    if (results[i].success) {
                        successes += results[i].transactionCount;
                    } else {
                        failedTransactionDates.push(results[i].transactionDate);
                        failures += results[i].transactionCount;
                    }
                }

                let resultObject = {
                    accountId,
                    successes,
                    failures
                };
                if (failures > 0) {
                    resultObject.failedTransactionDates = failedTransactionDates;
                }

                resolve(resultObject);
            });
        });
    },
    addOrUpdateEntry: ({ accountId, transactionDate, transactions }) => {
        return new Promise((resolve, reject) => {
            console.log(`addOrUpdateEntry called for transaction date '${transactionDate}' with ${JSON.stringify(transactions)}`);
            let transactionCount = getObjectLength(transactions);
            // look for existing transaction
            self.getEntry({ accountId, transactionDate })
            .then((existingCollection) => {
                let mode = existingCollection ? 'update' : 'insert';
                console.log(`${mode} transactions for date ${transactionDate}`);

                let resolveSuccess = () => {
                    console.log(`${mode} transaction date ${transactionDate} succeeded`)
                    resolve({
                        transactionDate: transactionDate.toString(),
                        success: true,
                        mode,
                        transactionCount
                    });
                };
                let resolveFailure = (err) => {
                    err = JSON.stringify(err);
                    console.error(`${mode} transaction date ${transactionDate} failed: ${err}`);
                    resolve({
                        transactionDate: transactionDate.toString(),
                        success: false,
                        mode,
                        error: err,
                        transactionCount
                    });
                }

                // insert or update accordingly
                if (existingCollection) {
                    console.log(`transactions for date ${transactionDate} found, updating`);
                    // merge transactions, overwrites existing transactions with matching IDs
                    for (let key in transactions) {
                        existingCollection.transactions[key] = transactions[key];
                    }
                    // switch transactions for update
                    transactions = existingCollection.transactions;
                }

                dynamodb.put({
                    TableName: TRANSACTIONS_TABLE,
                    Item: {
                        accountId,
                        transactionDate,
                        transactions
                    }
                }).promise()
                .then(resolveSuccess)
                .catch(resolveFailure);
            })
            .catch((err) => {
                console.error(`error looking up transactions for date ${transactionDate}: ${JSON.stringify(err)}`);
                resolve({ transactionDate, success: false, error: err });
            });
        });
    },
    getEntry: ({ accountId, transactionDate }) => {
        return new Promise((resolve, reject) => {
            console.log(`transactions.getEntry called for account ${accountId} with date ${transactionDate}`);

            dynamodb.get({
                TableName: TRANSACTIONS_TABLE,
                Key: {
                    "accountId": accountId,
                    "transactionDate": transactionDate
                }
            }).promise()
            .then(result => {
                result = result.Item;
                console.log(`query result: ${JSON.stringify(result)}`);
                resolve(result);
            })
            .catch(reject);
        });
    },
    getEntriesInRange: ({ accountId, startDate, endDate }) => {
        return new Promise((resolve, reject) => {
            console.log(`transactions.getEntriesInRange called for account ${accountId} with dates ${startDate}-${endDate}`);

            dynamodb.query({
                TableName: TRANSACTIONS_TABLE,
                KeyConditionExpression: 'accountId = :accountId and transactionDate between :startDate and :endDate',
                ExpressionAttributeValues: {
                  ':accountId': accountId,
                  ':startDate': Number(startDate),
                  ':endDate': Number(endDate)
                }
            }).promise()
            .then(result => {
                console.log(`query result: ${JSON.stringify(result)}`);
                resolve({
                    success: true,
                    query: `${accountId}: ${startDate}-${endDate}`,
                    resultCollection: result.Items
                });
            })
            .catch((err) => {
                resolve({
                    success: false,
                    error: err
                });
            });
        });
    },
    deleteEntriesInRange: ({ accountId, startDate, endDate }) => {
        return new Promise((resolve, reject) => {
            // ensure date range ordered correctly
            let nextDate = Number(startDate);
            endDate = Number(endDate);
            if (nextDate > endDate) {
                nextDate = endDate;
                endDate = Number(startDate);
            }
            console.log(`transactions.deleteEntriesInRange called for account ${accountId} with dates ${nextDate}-${endDate}`);

            let promises = [];
            while (nextDate <= endDate) {
                // delete the day's transactions
                console.log(`deleting transactions for date ${nextDate}`);
                promises.push(
                    dynamodb.delete({
                        TableName: TRANSACTIONS_TABLE,
                        Key: {
                            "accountId": accountId,
                            "transactionDate": nextDate
                        }
                    }).promise()
                );
                // increment the nextDate
                nextDate = Number(
                    moment(`${nextDate}`, 'YYYYMMDD')
                    .add(1, 'd')
                    .format('YYYYMMDD')
                );
            }

            Promise.all(promises)
            .then(() => {
                resolve({
                    success: true
                });
            })
            .catch((err) => {
                resolve({
                    success: false,
                    error: err
                });
            });
        });
    },
};

module.exports = self;