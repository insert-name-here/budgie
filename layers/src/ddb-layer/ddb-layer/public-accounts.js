const aws = require('aws-sdk');
const dynamodb = new aws.DynamoDB.DocumentClient();

const PUBLIC_ACCOUNTS_TABLE = process.env.PUBLIC_ACCOUNTS_TABLE_NAME;

const MASTER_PAGE_NUMBER = -1;
const MINIMUM_ACCOUNTS_THRESHOLD = 1;
const PREFERRED_ACCOUNTS_NUMBER = 3;

let self = {
    addEntry: ({ accountId }) => {
        return new Promise((resolve, reject) => {
            console.log(`public-accounts.addEntry called for ${accountId}`);

            // TODO for now, just use page 1
            //      in future, adding an entry above the threshold of the last
            //      page should split the last page and add it to the new
            //      last page
            let pageNumber = 1;

            self.getPage(pageNumber)
            .then(page => {
                console.log(`adding account ${accountId} to page ${page.pageNumber}`);

                let index = page.accounts.indexOf(accountId);
                if (index >= 0) {
                    console.log(`accountId already found on page ${page.pageNumber}`);
                    resolve();
                    return;
                }

                page.accounts.push(accountId);
                dynamodb.update({
                    TableName: PUBLIC_ACCOUNTS_TABLE,
                    Key: {
                        pageNumber: page.pageNumber
                    },
                    UpdateExpression: "set accounts = :accounts",
                    ExpressionAttributeValues:{
                        ":accounts": page.accounts
                    }
                }).promise()
                .then(resolve)
                .catch(reject);

            })
            .catch(reject);
        });
    },
    removeEntry: ({ accountId }) => {
        return new Promise((resolve, reject) => {
            console.log(`public-accounts.removeEntry called for ${accountId}`);

            // TODO for now, just use page 1
            //      in future, removing an entry below the threshold of the
            //      page should merge the page with the last page and remove
            //      the last page
            let pageNumber = 1;

            self.getPage(pageNumber)
            .then(page => {
                let index = page.accounts.indexOf(accountId);
                if (index < 0) {
                    console.log(`accountId not found on page ${page.pageNumber}`);
                    resolve();
                    return;
                }
                console.log(`removing account ${accountId} from page ${page.pageNumber}`);
                page.accounts.splice(index, 1);
                dynamodb.update({
                    TableName: PUBLIC_ACCOUNTS_TABLE,
                    Key: {
                        pageNumber: page.pageNumber
                    },
                    UpdateExpression: "set accounts = :accounts",
                    ExpressionAttributeValues:{
                        ":accounts": page.accounts
                    }
                }).promise()
                .then(resolve)
                .catch(reject);

            })
            .catch(reject);
        });
    },
    getAccounts: () => {
        return new Promise((resolve, reject) => {
            console.log(`public-accounts.getAccounts called`);
            // TODO pageNum should be determined by picking a random page
            //      from the masterPage
            let pageNumber = 1;
            self.getPage(pageNumber)
            .then(page => {
                let len = page.accounts.length;
                // fail if we don't meet the threshold
                if (len < MINIMUM_ACCOUNTS_THRESHOLD) {
                    let msg = `query failed: minimum account threshold not met`;
                    console.error(`${msg} on page ${pageNumber}`);
                    reject(new Error(msg));
                    return;
                }
                // return all accounts if we don't have more than the preferred number of accounts
                if (len <= PREFERRED_ACCOUNTS_NUMBER) {
                    resolve(page.accounts);
                    return;
                }
                // take a slice of the accounts array
                // pick a random index between 0 and len - PREFERRED_ACCOUNTS_NUMBER
                // 0 - (arr.length - preferred) (inclusive)
                let start = Math.floor(Math.random() * (arr.length + 1 - PREFERRED_ACCOUNTS_NUMBER));
                let end = start + PREFERRED_ACCOUNTS_NUMBER;
                resolve(page.accounts.slice(start, end));
            })
            .catch(reject);
        });
    },
    getPage: (pageNumber) => {
        return new Promise((resolve, reject) => {
            console.log(`public-accounts.getPage called for page ${pageNumber}`);
            dynamodb.get({
                TableName: PUBLIC_ACCOUNTS_TABLE,
                Key: {
                    pageNumber
                }
            }).promise()
            .then(result => {
                if (!result.Item) {
                    console.log(`creating page ${pageNumber}`);
                    let page = {
                        pageNumber,
                        accounts: []
                    };

                    dynamodb.put({
                        TableName: PUBLIC_ACCOUNTS_TABLE,
                        Item: page
                    }).promise()
                    .then(() => {
                        // update master page
                        updateMasterPage(page);
                    })
                    .catch(reject);
                } else {
                    resolve(result.Item);
                }
            })
            .catch(reject);

            let updateMasterPage = (page) => {
                console.log(`updating the master page`);
                self.getMasterPage()
                .then((masterPage) => {
                    masterPage.pages.push(page.pageNumber);
                    dynamodb.update({
                        TableName: PUBLIC_ACCOUNTS_TABLE,
                        Key: {
                            pageNumber: MASTER_PAGE_NUMBER
                        },
                        UpdateExpression: "set pages = :pages",
                        ExpressionAttributeValues:{
                            ":pages": masterPage.pages
                        }
                    }).promise()
                    .then(() => {
                        resolve(page);
                    })
                    .catch(reject);
                })
                .catch(reject);
            };
        });
    },
    getMasterPage: () => {
        return new Promise((resolve, reject) => {
            console.log(`public-accounts.getMasterPage called`);
            dynamodb.get({
                TableName: PUBLIC_ACCOUNTS_TABLE,
                Key: {
                    pageNumber: MASTER_PAGE_NUMBER
                }
            }).promise()
            .then(result => {
                if (!result.Item) {
                    console.log(`creating master page`);
                    let masterPage = {
                        pageNumber: MASTER_PAGE_NUMBER,
                        pages: []
                    };

                    dynamodb.put({
                        TableName: PUBLIC_ACCOUNTS_TABLE,
                        Item: masterPage
                    }).promise()
                    .then(() => { resolve(masterPage); })
                    .catch(reject);
                } else {
                    resolve(result.Item);
                }
            })
            .catch(reject);
        });
    }
};

module.exports = self;