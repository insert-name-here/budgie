const moment = require('moment');
const transactions = require('/opt/nodejs/ddb-layer/transactions');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');
const uuid = require('uuid').v4;

exports.deleteTransactions = async (event) => {
    return new Promise((resolve, reject) => {
        users.authenticate(event)
        .then((user) => {
            // get query parameters from querystring
            let accountId = null;
            if (event.pathParameters && event.pathParameters.accountId) {
                accountId = event.pathParameters.accountId;
            } else {
                if (event.queryStringParameters && event.queryStringParameters.accountId) {
                    accountId = event.queryStringParameters.accountId;
                }
            }

            // validate query parameters
            let startDate, endDate;
            try {
                startDate = event.queryStringParameters.startDate;
                endDate = event.queryStringParameters.endDate;
            } catch (e) {
                console.error(e);
            }
            if (!startDate && !endDate) {
                resolve(utils.formatErrorResponse(
                    400, new Error(`startDate and endDate querystring parameters must be provided`)
                ));
            }

            if (!(
                startDate &&
                endDate &&
                moment(startDate, 'YYYYMMDD', true).isValid() &&
                moment(endDate, 'YYYYMMDD', true).isValid()
                )) {
                resolve(utils.formatErrorResponse(
                    400, new Error(`startDate and endDate must be in the format YYYYMMDD`)
                ));
                return;
            }

            // create array of accountIds to query
            let accountIds = [];
            if (accountId) {
                // single account id specified
                if (user.accounts.indexOf(accountId) < 0) {
                    resolve(utils.formatErrorResponse(
                        401,
                        new Error(`account ${accountId} not found in user accounts`)
                    ));
                    return;
                }
                accountIds.push(accountId);
            } else {
                // all of user's accounts
                accountIds = user.accounts || [];
            }
            // if no accounts available return 404
            if (accountIds.length == 0) {
                resolve(utils.formatErrorResponse(
                    404,
                    null,
                    `No accounts found.`
                ));
                return;
            }

            let promises = [];
            for (var a in accountIds) {
                let accountId = accountIds[a];
                promises.push(transactions.deleteEntriesInRange({ accountId, startDate, endDate }));
            }

            Promise.all(promises)
            .then(results => {
                // collect results as array of transactions
                for (let r in results) {
                    let result = results[r];
                    if (!result.success) {
                        console.error(`${result.query} error:`);
                        console.error(result.error);
                        resolve(utils.formatErrorResponse(
                            500,
                            result.error,
                            `Error deleting transactions.`
                        ));
                        return;
                    }
                }

                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            })
            .catch((err) => {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Error querying transactions.`
                ));
            });
        })
        .catch((err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Authentication failed, userId / authKey mismatch.`
            ));
        })
   });
}
