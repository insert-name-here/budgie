const transactions = require('/opt/nodejs/ddb-layer/transactions');
const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

exports.uploadTransactions = async (event) => {
    return new Promise((resolve, reject) => {
        users.authenticate(event)
        .then((user) => {
            // parse body, which is an array of transaction rows
            let payload = null;
            try {
                payload = JSON.parse(event.body);
                for (var accountId in payload) {
                    // ensure accountId in user's accounts
                    if (!user.accounts || user.accounts.indexOf(accountId) < 0) {
                        resolve(utils.formatErrorResponse(
                            401,
                            new Error(`account ${accountId} not found in user accounts`)
                        ));
                        return;
                    }
                    if (!Array.isArray(payload[accountId])) {
                        throw new Error('account key must be assigned an array of transactions');
                    }
                }
            } catch (err) {
                resolve(utils.formatErrorResponse(400, err));
                return;
            }

            let promises = [];
            for (var accountId in payload) {
                promises.push(
                    transactions.addOrUpdateEntries({
                        accountId,
                        transactions: payload[accountId]
                    })
                );
            }

            Promise.all(promises)
            .then(results => {
                // each result will include a results object, successes and failures
                let successes = 0;
                let failures = 0;
                for (let i in results) {
                    successes += results[i].successes;
                    failures += results[i].failures;
                }

                // initialize to 206 (partial content)
                let success = true;
                if (successes == 0 || failures > 0) {
                    success = false
                }

                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        success,
                        results,
                        successes,
                        failures
                    }
                }));
            })
            .catch((err) => {
                console.error(err);
                resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Error adding transactions.`
                ));
            });
        })
        .catch((err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Authentication failed, userId / authKey mismatch.`
            ));
        })
   });
}