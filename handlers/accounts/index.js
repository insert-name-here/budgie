const users = require('/opt/nodejs/ddb-layer/users');
const utils = require('/opt/nodejs/utility-layer/utils');

exports.createAccount = async (event) => {
    return new Promise((resolve, reject) => {
        users.authenticate(event)
        .then((user) => {
            users.createAccount({ user })
            .then((newAccountId) => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true,
                        "message": `Your new account ID is ${newAccountId}.`
                    }
                }));
            })
            .catch((err) => {
                resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `Error creating new account.`
                ));
            });
        })
        .catch((err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Authentication failed, userId / authKey mismatch.`
            ));
        })
    });
}

exports.getAccounts = async (event) => {
    return new Promise((resolve, reject) => {
        users.authenticate(event)
        .then((user) => {
            resolve(utils.createResponse({
                "statusCode": 200,
                "body": user.accounts || []
            }));
        })
        .catch((err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Authentication failed, userId / authKey mismatch.`
            ));
        })
    });
}

exports.updateAccountPrivacy = async (event) => {
    return new Promise((resolve, reject) => {
        users.authenticate(event)
        .then((user) => {
            let accountId = event.pathParameters.accountId;
            // check that accountId belongs to user
            if (user.accounts.indexOf(accountId) < 0) {
                resolve(utils.formatErrorResponse(
                    401,
                    new Error(`account ${accountId} not found in user accounts`)
                ));
                return;
            }
            // parse body for "accountStatus" with values "public" or "private"
            try {
                payload = JSON.parse(event.body);
                if (!payload.accountStatus) {
                    throw new Error(`accountStatus must be provided`);
                }
                if (["public", "private"].indexOf(payload.accountStatus) < 0) {
                    throw new Error(`accountStatus must be either "public" or "private"`);
                }
            } catch (err) {
                resolve(utils.formatErrorResponse(400, err));
                return;
            }

            let isPrivacyToBeEnabled = (payload.accountStatus == "private");
            console.log(`isPrivacyToBeEnabled: ${isPrivacyToBeEnabled}`);

            users.updateAccountPrivacy({ user, accountId, isPrivacyToBeEnabled })
            .then(() => {
                resolve(utils.createResponse({
                    "statusCode": 200,
                    "body": {
                        "success": true
                    }
                }));
            })
            .catch((err) => {
                resolve(utils.formatErrorResponse(
                    500,
                    err,
                    `An unexpected error occurred, account privacy status update failed.`
                ));
            });
        })
        .catch((err) => {
            console.error(err);
            resolve(utils.formatErrorResponse(
                401,
                err,
                `Authentication failed, userId / authKey mismatch.`
            ));
        })
    });
}